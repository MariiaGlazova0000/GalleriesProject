FROM java:8

WORKDIR /usr/src/main/java

COPY . .

CMD ["mvn", "-B", "package", "--file", "pom.xml"]
