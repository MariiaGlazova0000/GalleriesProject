import entity.Studio;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.Scanner;

public class Application {
    private static final Logger l = Logger.getLogger(Application.class.getName());

    static EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("entityManager");
    static EntityManager entityManager = entityManagerFactory.createEntityManager();
    
    public static void main(String[] args) {
        Studio st = new Studio(1L, "Garrus Vakarian and Jane Shepard");
        l.info(st);
        System.exit(1);
    }
}
